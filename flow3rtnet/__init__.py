try:
    from typing import Callable, Optional # mainly for IDE, not present in micropython
except:
    pass

import network
import machine
import time
import leds
import socket

import sys

from st3m.application import Application, ApplicationContext
from st3m.ui.view import BaseView, ViewManager
from st3m.ui.elements.menus import SimpleMenu
from st3m.ui.menu import (
    MenuItem,
    MenuItemBack,
    MenuItemForeground,
    MenuItemNoop,
    MenuItemAction,
    MenuItemLaunchPersistentView,
)
from st3m.input import InputState
from ctx import Context
import st3m.run

class MenuItemActionUpdate(MenuItemAction):
    """
    A MenuItem which runs the provided lambda action and updates its label with the return value.
    """

    def __init__(self, label: str, action: Callable[[], None]) -> None:
        self._label = label
        self._action = action

    def press(self, vm: Optional[ViewManager]) -> None:
        self._label = self._action()

    def label(self) -> str:
        return self._label

class Flow3rtNet(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)

        self.ssid = "flow3rtnet"
        self.psk = "artnet11"
        self.olddata = [0] * 255
        self.universe = 0

        print("Initializing WiFi device")
        self.wlan = network.WLAN(network.STA_IF)
        self.wifi_menu = SimpleMenu(
            [
                MenuItemBack(),
                MenuItemActionUpdate(f"Active: {self.wlan.active()}", self._toggle_wifi_active),
                MenuItemActionUpdate(f"WiFi Connected: {self.wlan.isconnected()}", self._toggle_wifi_connected)
            ],
        )
        self.main_menu = SimpleMenu(
            [
                MenuItemBack(),
                MenuItemForeground("Settings", self.wifi_menu),
                MenuItemForeground("ArtNet viewer", ArtNetViewer(self))
            ],
        )

        # Try to import StupidArtnet library from bundle folder
        self.bundle_path = ""

        try:
            self.bundle_path = app_ctx.bundle_path
        except:
            pass

        if self.bundle_path == "":
            self.bundle_path = "/flash/sys/apps/flow3rtnet/"

        print(f"Bundle path: {self.bundle_path}")

        sys.path.append(self.bundle_path)
        from stupidArtnet import StupidArtnetServer

        print("Creating StupidArtnetServer")
        self.server = StupidArtnetServer()
        print("Creating listener")
        u1_listener = self.server.register_listener(self.universe, callback_function=self.parse_data)

    def _toggle_wifi_active(self):
        new_state = not self.wlan.active()
        print(f"Setting WiFi active to {new_state}")
        self.wlan.active(new_state)
        return f"Active: {self.wlan.active()}"

    def _toggle_wifi_connected(self):
        if not self.wlan.isconnected():
            print(f"Connecting to {self.ssid}")
            try:
                self.wlan.connect(self.ssid, self.psk)
                while not self.wlan.isconnected():
                    print("Not yet connected")
                    #machine.lightsleep(1000)
                    time.sleep_ms(1000)
                print("Connected!")
                while self.wlan.ifconfig()[0] == '0.0.0.0':
                    print("Waiting for DHCP")
                    #machine.lightsleep(1000)
                    time.sleep_ms(1000)

                ifconfig = self.wlan.ifconfig()
                print(f"DHCP successful. ifconfig: {ifconfig}")

                print("Creating StupidArtnet Client")
                from stupidArtnet import StupidArtnet
                broadcast_address = self._get_broadcast_addr(ifconfig[0], ifconfig[1])
                print(f"Broadcast address is {broadcast_address}")
                self.client = StupidArtnet(broadcast_address, self.universe, 512, 30, True, False)
                print("Created stupidArtnet client:", self.client)
            except Exception as e:
                print(f"Exception while setting up WiFi: {e}")
        else:
            self.wlan.disconnect()

        return f"Connected: {self.wlan.isconnected()}"

    def _get_broadcast_addr(self, ip_addr, subnet_mask):
        ip_binary = self._inet_pton(ip_addr)
        subnet_binary = self._inet_pton(subnet_mask)
        broadcast_binary = (ip_binary & subnet_binary) | (0xffffffff & ~subnet_binary)
        return self._inet_ntop(broadcast_binary)

    def _inet_pton(self, ip_addr_str):
        octets = ip_addr_str.split('.')
        ip_addr_bin = 0
        for i, octet in enumerate(octets):
            ip_addr_bin |= (int(octet) << ((3-i)*8))
        return ip_addr_bin

    def _inet_ntop(self, ip_addr_bin):
        octets = [0, 0, 0, 0]
        for i in range(len(octets)):
            octets[i] = (ip_addr_bin >> ((3-i)*8)) & 0xff

        return '.'.join([str(octet) for octet in octets])


    # create a callback to handle data when received
    def parse_data(self, data, addr):
        # the received data is an array
        # of the channels value (no headers)
        if data == self.olddata:
            pass
        else:
            self.olddata = data
            num_data = len(data)

            print('Received new data \n', data)
            if num_data > 0:
                leds.set_brightness(data[0])

                r = 0
                g = 0
                b = 0

                if num_data > 1:
                    r = data[1] / 255
                    if num_data > 2:
                        g = data[2] / 255
                        if num_data > 3:
                            b = data[3] / 255

                leds.set_all_rgb(r, g, b)

                if num_data > 4:
                    offset = 4

                    num_rgb_values = len(data) - offset

                    num_leds = min(40, int(num_rgb_values / 3))

                    i = 0 # make sure i can be accessed even if the for loop doesn't run
                    for i in range(num_leds):
                        leds.set_rgb(i, data[offset + i * 3] / 255,
                                        data[offset + i * 3 + 1] / 255,
                                        data[offset + i * 3 + 2] / 255)

                    # If there is data for not all 3 channels of the last LED, still use it
                    num_remaining_channels = num_rgb_values % 3
                    if num_remaining_channels == 1:
                        leds.set_rgb(i, data[offset + i * 3] / 255, 0, 0)
                    if num_remaining_channels == 2:
                        leds.set_rgb(i, data[offset + i * 3] / 255,
                                        data[offset + i * 3 + 1] / 255,
                                        0)

                leds.update()

    def draw(self, ctx: Context) -> None:
        # Paint the background black
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()
        # Red square
        ctx.rgb(255, 0, 0).rectangle(-20, -20, 40, 40).fill()

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms) # Let Application do its thing
        self.vm.push(self.main_menu)

class ArtNetViewer(BaseView):
    def __init__(self, main_app):
        super().__init__()
        self.main_app = main_app
        self.addr = 0
        self.edit_mode = False
        self.step_size = 1

    def draw(self, ctx: Context) -> None:
        self.data = self.main_app.olddata[self.addr]
        data_str = str(self.data)
        addr_str = f"Channel: {self.addr}"

        # Paint the background black
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()

        # Visualize data value as colored square
        r = max(255 - max(self.data - 127, 0) * 2, 0) / 255
        g = min(self.data * 2, 255) / 255
        rectangle = ctx.rgb(r, g, 0).rectangle(-20, -20, 40, 40)
        if not self.edit_mode:
            rectangle.fill()

        # Write data value on top
        ctx.font_size = 15
        ctx.move_to(-0.5 * ctx.text_width(data_str), 0)
        ctx.rgb(0, 0, 0).text(data_str)

        # Write address value below
        ctx.font_size = 15
        ctx.move_to(-0.5 * ctx.text_width(addr_str), 35)
        ctx.rgb(255, 255, 255).text(addr_str)

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)  # Let Application do its thing
        if self.input.buttons.app.middle.pressed:
            self.edit_mode = not self.edit_mode
            self.step_size = 1

        elif self.edit_mode:
            newdata = self.main_app.olddata.copy()
            if self.input.buttons.app.left.pressed:
                newdata[self.addr] = max(newdata[self.addr] - self.step_size, 0)
            elif self.input.buttons.app.right.pressed:
                newdata[self.addr] = min(newdata[self.addr] + self.step_size, 255)

            if newdata != self.main_app.olddata:
                print(f"Trying to send {newdata} with length {len(newdata)}")
                self.main_app.client.set(newdata)

        elif not self.edit_mode:
            if self.input.buttons.app.left.pressed:
                self.addr -= self.step_size
            elif self.input.buttons.app.right.pressed:
                self.addr += self.step_size

        print(self.input.buttons.app.left.repeated)

if __name__ == '__main__':
    # Continue to make runnable via mpremote run.
    st3m.run.run_view(Flow3rtNet(ApplicationContext()))
